<?php 
/**
 * The template for displaying 404 pages (not found).
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
get_header(); ?>

		<div id="main" class="main-content" role="main">
			<div class="container">
				<section class="error404-content">
	
					<header class="page-header">
						<h1 class="page-title"><?php _e( 'Oops! Page not found', 'melissa' ); ?></h1>
					</header><!-- .page-header -->
					
					<div class="page-content">
						<p><?php _e( 'It looks like nothing was found at this location. Maybe try one another search?', 'melissa' ); ?></p>
						<?php get_search_form(); ?>
					</div>

				</section>
			</div>
		</div><!--END .main-content-->

<?php get_footer(); ?>