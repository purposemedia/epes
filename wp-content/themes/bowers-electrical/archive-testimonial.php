<?php
/**
 * Displays archive pages.
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
get_header(); ?>

		<main id="main" class="main-content" role="main">
			<div class="container">
				<?php if ( have_posts() ) : ?>

					<section>

							<?php
							$args = array( 'post_type' => 'testimonial', 'posts_per_page' => -1 );
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post();
							$imgURL = "";
							if (has_post_thumbnail()) {
								$imgURL = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
							}
							?>

								<div class="testimonial-item">

									<div class="testimonial-image" style="background-image:url('<?php echo $imgURL; ?>')"></div>

									<div class="testimonial-content">
										<p>"<?php the_field('testimonial'); ?>"</p>
										<p><b><?php the_field('name'); ?></b></p>
									</div>

								</div>

							<?php endwhile; ?>

							<?php melissa_content_nav( 'nav-below' ); ?>

						<?php else : ?>

							<?php get_template_part( 'no-results', 'index' ); ?>

						<?php endif; ?>

					</section>
				</div>

		</main><!--END .main-content-->

<?php get_footer(); ?>
