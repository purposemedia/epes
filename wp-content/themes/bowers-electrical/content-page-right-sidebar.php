<?php 
/**
 * The template for displaying all page content in page-right-sidebar.php
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
 ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
		wp_link_pages( array(
		'before' => '<div class="page-links">' . __( 'Pages:', 'melissa' ),
		'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
