<?php 
/**
 * Displays the content for index.php
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="post-entry-header">
		<h1 class="post-entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="post-entry-meta">
			<?php the_category( ', ' ); ?> <?php the_time('M j, Y'); ?>
			<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
			<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'melissa' ), __( '1 Comment', 'melissa' ), __( '% Comments', 'melissa' ) ); ?></span>
			<?php endif; ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="post-entry-content">
		<?php the_excerpt(); ?>
	</div><!-- .entry-content -->

	<footer class="post-entry-meta">
			<span class="tags-links">
				<?php the_tags('Tags: ', ', '); ?> 
			</span>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->
