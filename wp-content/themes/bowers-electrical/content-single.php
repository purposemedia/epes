<?php 
/**
 * Displays content for single.php
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
 ?>
 
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'melissa' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-meta">
		<?php the_category( ', ' ); ?> <?php the_time('M j, Y'); ?>
		<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
		<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'melissa' ), __( '1 Comment', 'melissa' ), __( '% Comments', 'melissa' ) ); ?></span>
		<?php endif; ?>

		<span class="tags-links">
			<?php the_tags('Tags: ', ', '); ?> 
		</span>
	</footer><!-- .entry-meta -->

</article><!-- #post-## -->