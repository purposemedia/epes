<?php 
/**
 * Displays the content for index.php
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="post-entry-header">
		<h1 class="post-entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="post-entry-meta">
			<?php the_category( ', ' ); ?> <?php the_time('M j, Y'); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="post-entry-content">
		<?php show_content_snippet($post, 200); ?>
		<a href="<?php the_permalink(); ?>" class="continue"><?php _e( 'Continue Reading ...', 'melissa' ); ?></a>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'melissa' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="post-entry-meta">
			<span class="tags-links">
				<?php the_tags('Tags: ', ', '); ?> 
			</span>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->
