<?php 
/**
 * The main footer for our theme.
 * Usually displays copyright details and credits and sometimes legal information 
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
?>

		<?php if (!is_front_page()) : ?>
		<div class="home-news footer-news">
			<div class="container">
				<h5><?php the_field('2nd_header', 5); ?></h5>
				
				<div>
			    <?php
				$args = array( 'post_type' => array( 'post', 'case-studies' ), 'posts_per_page' => 6, 'cat' => 5 );
				$postsmenu = get_posts( $args );
				foreach($postsmenu as $post) : setup_postdata($post); ?>
				
					<div class="<?php echo get_post_type( $post ) ?>-post news-block">
						<div class="image">
							<a href="<?php the_permalink(); ?>">
								<?php if (get_post_type( $post ) == 'post'): ?>
									<?php the_post_thumbnail('news-post'); ?>
								<?php else: ?>
									<?php the_post_thumbnail('case-post'); ?>
								<?php endif; ?>
							<h3>
								<?php if (get_post_type( $post ) == 'post'): ?>
									News:
								<?php else: ?>
									Case study:
								<?php endif; ?>

								<?php the_title(); ?>
							</h3>
							</a>
						</div>
						<div class="content">
							<a href="<?php the_permalink(); ?>"><?php show_content_snippet($post); ?></a>

							<!-- <p class="link">
							<a href="<?php the_permalink(); ?>">Read more &raquo;</a></p> -->
						</div>
					</div>

				<?php endforeach; ?>
				</div>
			</div>
		</div><!-- Home news -->
		<?php endif; ?>

		<footer id="colophon" class="main-footer" role="contentinfo">

			<div class="container">
				<div class="useful-links footer-col1">
					<h5>Useful links</h5>

					<?php melissa_footer_menu(); ?>
				</div>
				<div class="latest-tweets footer-col2">
					<h5>Latest tweets</h5>

					<?php dynamic_sidebar( 'Footer Tweets' ); ?>
				</div>
				<div class="contact-us footer-col3">
					<h5>Contact us</h5>

					<ul>
                        <li><i class="fa fa-phone"></i> <a href="tel:<?php the_field('phone_number', 'options'); ?>"><?php the_field('phone_number', 'options'); ?></a></li>
                        <li><i class="fa fa-envelope"></i> <a href="mailto:<?php the_field('email_address', 'options'); ?>"><?php the_field('email_address', 'options'); ?></a></li>
                        <li><i class="fa fa-map-marker"></i> Head office: <br><?php the_field('address', 'options'); ?></li>
                        <li><i class="fa fa-twitter"></i> <a href="<?php the_field('twitter_url', 'options'); ?>" target="_blank">@EPES_01</a></li>
                    </ul>
				</div>
			</div>
			
		</footer><!--END .main-footer-->

		<div class="copyright">
			<div class="container">
				<div class="left">
					<p>&copy; Bowers Electricals <?php echo date('Y'); ?>. A part of the Bowers Group of Companies.</p>
				</div>
				<div class="right">
					<p class="pm-link"><?php if( is_page_template('page-home.php') ): ?>eCommerce web design by 
					<a href="http://www.purposemedia.co.uk/" target="_blank" title="eCommerce web design">purpose media.</a> </p>
+                <?php endif; ?></p>
				</div>
			</div>
		</div><!-- copyright -->

		<div class="footer-awards">
			<div class="container">
				<?php //while ( have_posts() ) : the_post(); ?>
					<?php
						if( get_field('awards', 'options'))
						{
							while( has_sub_field('awards', 'options') )
							{
								$image = get_sub_field('image');
							?>
								<div class="award">
									<img src="<?php echo $image[url]; ?>" alt="<?php echo $image[alt]; ?>" />
								</div>
							<?php
							}
						}
					?>
				<?php //endwhile; // end of the loop. ?>
			</div>
		</div>
	
		<?php wp_footer(); ?>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/owl.carousel.min.js"></script>

		<!-- Add mousewheel plugin (this is optional) -->
	    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

	    <!-- Add fancyBox -->
	    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
	</body>
</html>
