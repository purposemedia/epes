<?php
/**
 *
 * The core file for the theme. This is where most of the functions and features reside.
 * Additional custom functions should be placed inside the /functions.php file, instead of here.
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 *
 *	- set $content_with
 *	- theme defaults
 *	- register widgetized area
 *	- return default wp_nav_menu for main nav
 *	- enqueue scripts and styles
 *	- custom admin login logo
 *	- add menu-last-item to wp_nav_menu
 *  - custom function for showing excerpts
 *  - random cleanup items
 *	- remove 'text/css' from enqueued stylesheet
 *	- change admin screen footer text
 *	- custom template tags for this theme.
 *	- custom functions that act independently of the theme templates.
 *	- customizer additions.
 *  - load TGM Plugin Activation for required/recommended plugins
 */

/*
| ------------------------------------------------------------------------
| Set the content width based on the theme's design and stylesheet.
| ------------------------------------------------------------------------
*/

if ( ! isset( $content_width ) )
	$content_width = 1068; /* pixels */

/*
| ------------------------------------------------------------------------
| Sets up theme defaults and registers support for various WordPress features.
| ------------------------------------------------------------------------
*/

if ( ! function_exists( 'melissa_setup' ) ) :

function melissa_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /translations/ directory
	 * If you're building a theme based on melissa, use a find and replace
	 * to change 'melissa' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'melissa', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 */
	add_theme_support( 'post-thumbnails' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary-navigation' => __( 'Main Menu', 'melissa' ),
		'second-navigation' => __( 'Top Menu', 'melissa' ),
		'footer-navigation' => __( 'Footer Menu', 'melissa' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
			'chat'               // chat transcript
		)
	);

	/**
	 * Add custom image sizes
	 */
	add_image_size( 'news-post', 175, 231, true );
	add_image_size( 'case-post', 350, 160, true );
	add_image_size( 'menu-image', 150, 150, true );

}
endif; // melissa_setup
add_action( 'after_setup_theme', 'melissa_setup' );

/*
| ------------------------------------------------------------------------
| Register widgetized area and update sidebar with default widgets.
| ------------------------------------------------------------------------
*/

function melissa_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'melissa' ),
		'id'            => 'blog-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => __( 'Page Sidebar', 'melissa' ),
		'id'            => 'page-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Tweets', 'melissa' ),
		'id'            => 'footer-sidebar',
		'before_widget' => '<aside class="twitter-widget">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
}
add_action( 'widgets_init', 'melissa_widgets_init' );

/*
| ------------------------------------------------------------------------
| Returns main navigation menu.
| ------------------------------------------------------------------------
*/

function melissa_main_menu() {

	$melissa_main_menu_args = array(
	'theme_location'  => 'primary-navigation',
	'menu'            => 'Main Menu',
	'container'       => '',
	'container_class' => '',
	'container_id'    => '',
	'menu_class'      => '',
	'menu_id'         => '',
	'echo'            => true,
	'fallback_cb'     => 'wp_page_menu',
	'before'          => '',
	'after'           => '',
	'link_before'     => '',
	'link_after'      => '',
	'items_wrap'      => '<ul id="%1$s" class="%2$s"><li class="search-menu"><a href="#searchbox" class="fancybox"><i class="fa fa-search"></i></a></li>%3$s</ul>',
	'depth'           => 0,
	'walker'          => ''
	);

	wp_nav_menu( $melissa_main_menu_args );
}

function melissa_top_menu() {

	$melissa_top_menu_args = array(
	'theme_location'  => 'second-navigation',
	'menu'            => 'Top Menu',
	'container'       => '',
	'container_class' => '',
	'container_id'    => '',
	'menu_class'      => '',
	'menu_id'         => '',
	'echo'            => true,
	'fallback_cb'     => 'wp_page_menu',
	'before'          => '',
	'after'           => '',
	'link_before'     => '',
	'link_after'      => '',
	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
	'depth'           => 0,
	'walker'          => ''
	);

	wp_nav_menu( $melissa_top_menu_args );
}

function melissa_footer_menu() {

	$melissa_footer_menu_args = array(
	'theme_location'  => 'footer-navigation',
	'menu'            => 'Footer Menu',
	'container'       => '',
	'container_class' => '',
	'container_id'    => '',
	'menu_class'      => '',
	'menu_id'         => '',
	'echo'            => true,
	'fallback_cb'     => 'wp_page_menu',
	'before'          => '',
	'after'           => '',
	'link_before'     => '',
	'link_after'      => '',
	'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
	'depth'           => 0,
	'walker'          => ''
	);

	wp_nav_menu( $melissa_footer_menu_args );
}

/*
| ------------------------------------------------------------------------
| Enqueue scripts and styles.
| ------------------------------------------------------------------------
*/

function melissa_scripts() {
	// register and enqueue styles
	wp_register_style( 'font-awesome', get_stylesheet_directory_uri() . '/css/font-awesome/css/font-awesome.min.css', array(), 'all' );

	wp_enqueue_style( 'melissa-style', get_stylesheet_directory_uri() . '/style.css', array('font-awesome'), '', 'all' );

	//register scripts
    wp_register_script( 'melissa-modernizr', get_stylesheet_directory_uri() . '/js/libs/modernizr.2.6.2.min.js', array(), false );
    wp_register_script( 'melissa-selectivizr', get_stylesheet_directory_uri() . '/js/libs/selectivizr-min.js', array(), false );
    wp_register_script( 'melissa-skip-link-focus-fix', get_stylesheet_directory_uri() . '/js/skip-link-focus-fix.js', array( 'jquery' ), true );
    wp_register_script( 'melissa-plugins', get_stylesheet_directory_uri() . '/js/plugins.js', array( 'jquery' ), true );
    wp_register_script( 'melissa-custom', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ), true );
    wp_register_script( 'mobile-menu', get_stylesheet_directory_uri() . '/js/mobile-menu.js', array( 'jquery' ), true );

	// enqueue scripts
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'melissa-modernizr' );
	wp_enqueue_script( 'melissa-selectivizr' );
	wp_enqueue_script( 'melissa-skip-link-focus-fix' );
	wp_enqueue_script( 'melissa-plugins' );
	wp_enqueue_script( 'melissa-custom' );
    wp_enqueue_script( 'mobile-menu' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'melissa-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}

}
add_action( 'wp_enqueue_scripts', 'melissa_scripts' );

/*
| ------------------------------------------------------------------------
| Returns a custom admin login logo insetad of the WordPress logo.
| ------------------------------------------------------------------------
*/

function melissa_change_wp_login_logo() {
echo "
<style>
body.login #login h1 a {
background: url('".get_bloginfo('template_url')."/img/login-logo.png') 8px 0 no-repeat transparent;
height:100px;
width:320px; }
</style>
";
}
add_action("login_head", "melissa_change_wp_login_logo");

/*
| ------------------------------------------------------------------------
| Adds a class of .menu-last-item to all instances of wp_nav_menu().
| ------------------------------------------------------------------------
*/

function melissa_add_menu_last_item_class($menuHTML) {
$last_items_ids  = array();
$items_add_class = array();
$replacement = array();
$menus = wp_get_nav_menus();
foreach ( $menus as $menu_maybe ) {
        if ( $menu_items = wp_get_nav_menu_items($menu_maybe->term_id) ) {
            $items = array();
            foreach ( $menu_items as $menu_item )
            {
                if( isset( $items[$menu_item->menu_item_parent] ) )
                {
                                $items[$menu_item->menu_item_parent][] .= $menu_item->ID;
                }
                else
                {
                                $items[$menu_item->menu_item_parent][] = $menu_item->ID;
                }
            }
            foreach ( $items as $item )
            {
                            $last_items_ids[] .= end($item);
            }
        }
	}
	foreach( $last_items_ids as $last_item_id ) {
	                $items_add_class[] .= ' menu-item-'.$last_item_id;
	                $replacement[]     .= ' menu-item-'.$last_item_id . ' menu-last-item';
	}
	$menuHTML = str_replace($items_add_class, $replacement, $menuHTML);
	return $menuHTML;
}
add_filter('wp_nav_menu','melissa_add_menu_last_item_class');

/*
| ------------------------------------------------------------------------
| Show Content - Gets content based on Excerpt, <!--more--> Content or Substring of Content.
| ------------------------------------------------------------------------
*/

function show_content_snippet( $post, $amount = 90 ) {
	$post = (array)$post;
	if( $post['post_excerpt'] !== '' ) {
		echo apply_filters( 'the_excerpt', $post['post_excerpt'] );
	}elseif( preg_match( '#<!--more-->#is', $post['post_content'] ) ) {
		$moreSplit = explode( '<!--more-->', apply_filters( 'the_content', $post['post_content'] ) );
		echo $moreSplit[0];
	}else{
		echo '<p>' . substr( strip_tags( $post['post_content'] ), 0, $amount ) . ' <span>&raquo;</span></p>';
	}
}

/*
| ------------------------------------------------------------------------
| Random cleanup items
| ------------------------------------------------------------------------
*/

// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
function melissa_filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

// This removes the annoying […] to a Read More link
function melissa_excerpt_more($more) {
	global $post;
	// edit here if you like
return '...  <a class="excerpt-read-more" href="'. get_permalink($post->ID) . '" title="'. __('Read', 'melissa') . get_the_title($post->ID).'">'. __('Read more &raquo;', 'melissa') .'</a>';
}

/*
| ------------------------------------------------------------------------
| Remove 'text/css' from our enqueued stylesheet
| ------------------------------------------------------------------------
*/

function melissa_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

add_filter('style_loader_tag', 'melissa_style_remove');

/*
| ------------------------------------------------------------------------
| Custom template tags for this theme.
| ------------------------------------------------------------------------
*/

require get_template_directory() . '/inc/template-tags.php';

/*
| ------------------------------------------------------------------------
| Custom functions that act independently of the theme templates.
| ------------------------------------------------------------------------
*/

require get_template_directory() . '/inc/extras.php';

/*
| ------------------------------------------------------------------------
| Customizer additions.
| ------------------------------------------------------------------------
*/

require get_template_directory() . '/inc/customizer.php';


/*
| ------------------------------------------------------------------------
| Custom Post Types
|
| Tip! Move into plugin when deploying theme so user loses no data when changing themes.
|
| scpt: Add a filter for after_setup_theme and check to see if this plugin exists
| cpt: 	Add a new Super Custom Post Type
| tax: 	Add a new Super Custom Taxonomy
| amb: 	Add a new meta box for your Super Custom Post Type
| ctat: Connect one or more Post Types with one or more Taxonomies
| icon: Add an icon for your Custom Post Type
| cpm: 	Create a new Super_Custom_Post_Meta object around an existing post type (e.g. posts)| ------------------------------------------------------------------------
*/




