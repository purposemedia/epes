<?php 
/**
 * The header for our theme.
 * Usually displays the site's branding and main navigation
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
    <head>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TH6FHRPP');</script>
        <!-- End Google Tag Manager -->

        <!-- Define charset, usually UTF-8 -->
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <!-- Google Chrome Frame for IE -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <!-- Site title -->
        <title><?php wp_title( '|', true, 'right' ); ?></title> 
        <!-- mobile meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the img directory -->
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/img/apple-icon-touch.png">
        <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
        <!--[if IE]>
            <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
        <![endif]-->
        <!-- or, set /favicon.ico for IE10 win -->
        <meta name="msapplication-TileColor" content="#f01d4f">
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/win8-tile-icon.png">
        <!-- XFN 1.1 relationships meta -->
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <!-- link for the Pingback URL of your site. -->
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css">
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,400italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <?php wp_head(); ?>

    </head>
    <body <?php body_class(); ?>>

        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TH6FHRPP"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
         
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="header-contact">
            <div class="container">
                <div class="details">
                    <ul>
                        <li><i class="fa fa-phone"></i> <a href="tel:<?php the_field('phone_number', 'options'); ?>"><span class="desk"><?php the_field('phone_number', 'options'); ?></span> <span class="mob">Call</span></a></li>
                        <li><i class="fa fa-envelope"></i> <a href="mailto:<?php the_field('email_address', 'options'); ?>"><span class="desk"><?php the_field('email_address', 'options'); ?></span> <span class="mob">Email</span></a></li>
                        <li><i class="fa fa-comments"></i> <a href="<?php echo esc_url( home_url( '/' ) ); ?>contactus/"><span class="desk">Submit a web enquiry</span> <span class="mob">Message</span></a></li>
                    </ul>
                </div>
                <div class="top-menu">
                        <?php melissa_top_menu(); ?>
                </div>
            </div>
        </div>
        
        <header id="masthead" class="main-header" role="banner">
            <div class="container rel-menu">
                <div class="main-logo">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.jpg" alt="Bowers Electrical" class="header-logo" /></a>
                </div>

                <nav id="primary-navigation" class="main-menu" role="navigation">
                    <p><?php bloginfo('description'); ?></p>
                    <?php melissa_main_menu(); ?>
                </nav>


                <div class="mobile-menu">
                    <div class="mobile-menu__toggle">
                        <div class="mobile-menu__wrapper">
                            <a id="nav-icon2" class="nav-icon">
                                <div class="b-icon">
                                </div>
                            </a>
                            <h3 id="menu-open">MENU</h3>
                            <h3 id="menu-close">CLOSE</h3>
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.jpg" alt="Bowers Electrical" /></a>
                        </div>
                        <div id="mobile-menu-menu" class="mobile-menu__menu">
                            <?php
                                $melissa_main_menu_args = array(
                                    'theme_location'  => 'primary-navigation',
                                    'menu'            => 'Main Menu',
                                    'container'       => '',
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => '',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    'fallback_cb'     => 'wp_page_menu',
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'depth'           => 0,
                                    'walker'          => ''
                                );

                                wp_nav_menu( $melissa_main_menu_args );

                            ?>
                        </div>
                    </div>
                </div>

            </div>
            <div style="display: none; height: 40px;" class="fancybox-hidden">
                <div id="searchbox" style="height: 40px;">
                    <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <input type="search" class="search-field home-search" placeholder="<?php echo esc_attr_x( 'Search and hit enter &hellip;', 'placeholder', 'melissa' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'melissa' ); ?>">
                    </form>
                </div>
            </div>

        </header><!--END .main-header-->

        <?php if (!is_front_page()) : ?>
        <div class="page-title-wrapper">
            <?php if (get_field('page_background_image')) : ?>
                <img src="<?php the_field('page_background_image', $wp_query->post->ID); ?>" alt="Page background image" />
            <?php else: ?>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/page_header-new.jpg" alt="Default page background image" />
            <?php endif; ?>
            <div class="container">
                <?php if (is_home()) : ?>
                    <?php $blog_title = get_the_title( get_option(' page_for_posts ', true) ); ?>
                    <h1 class="page-title"><?php printf($blog_title); ?></h1>
                <?php elseif (is_post_type_archive()) : ?>
                    <h1><?php post_type_archive_title(); ?></h1>
                <?php elseif (is_category()) : ?>
                    <h1><?php single_cat_title(); ?></h1>
                <?php elseif (is_month()) : ?>
                    <h1>Monthly Archives</h1>
                <?php else : ?>
                    <h1><?php the_title(); ?></h1>
                <?php endif; ?>
            </div>
            <div class="breadcrumb-wrapper">
                <div class="container">
                    <div class="breadcrumbs">
                        <?php if ( function_exists('yoast_breadcrumb') ) {
                            yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                        } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>