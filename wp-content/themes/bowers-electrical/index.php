<?php 
/**
 * The main template file required by a WordPress theme.
 * Usually reserved for blog post entries but provides a
 * fallback template for pages without a more specific template.
 * For more information on template hierarchy see http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
get_header(); ?>

		<div id="main" class="main-content" role="main">
			<div class="container">

					<section class="entries-list">

						<?php if ( have_posts() ) : ?>

							<?php /* Start the Loop */ ?>
							<?php while ( have_posts() ) : the_post(); ?>

								<?php
									/* Include the Post-Format-specific template for the content.
									 * If you want to override this in a child theme, then include a file
									 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
									 */
									get_template_part( 'content', get_post_format() );
								?>

							<?php endwhile; ?>

							<?php melissa_content_nav( 'nav-below' ); ?>

						<?php else : ?>

							<?php get_template_part( 'no-results', 'index' ); ?>

						<?php endif; ?>


					</section>

					<?php get_sidebar(); ?>
			</div>
		</div><!--END .main-content-->

<?php get_footer(); ?>