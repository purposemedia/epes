( function( $, window ) {
    $.fn.extend({
        sync : function( $opt ) {
            var elms = [];
            function int( $el ) {
                var $selectors = $( $opt.selectors, $el ),
                $height = 0;
                $selectors.each( function() {
                    // So we can remove inline height styles to run the sync again at a later date
                    $(this).attr( 'style', ' ' );
                    if( $(this).height() > $height ) {
                        $height = $(this).height();
                    }
                });
                $selectors.height( $height );
            }
            function reSync() {
                for( var i in elms ) {
                    int( elms[i] );
                }
            }
            // Re-sync on window resize
            $(window).resize( reSync );
            return this.each( function() {
                elms.push( $(this) );
                int( $(this) );
            });
        }
    });
})( jQuery, window );

jQuery(document).ready(function($) {
    syncTestimonials();
    $(window).resize(function(){
        syncTestimonials();
    });
    function syncTestimonials(){
        var container = $('.testimonial-item').first().parent();
        var height = 0;
        if(!container.is('section')){
            return;
        }
        $('.testimonial-item').css('height', 'auto');
        $('.testimonial-content p:first-child').css('height', 'auto');
        container.find('.testimonial-item').each(function(){
            if($(this).height() > height){
                height = $(this).height();
            }
        });
        container.find('.testimonial-item').height(height);
        var contentHeight = 0;
        container.find('.testimonial-content').each(function(){
            if($(this).find('p').first().height() > contentHeight){
                contentHeight = $(this).find('p').first().height();
            }
        });
        container.find('.testimonial-content p:first-child').height(contentHeight);
    }
    //product text tabs
    $( "#tabs" ).tabs({ active : 0 });

	var menu = $('#menu-main-menu');

	$('li', menu).each( function()
	{
		if( $('> ul', $(this)).length !== 0 )
		{
			$(this).addClass('parent');
			$('> a', $(this)).append( '<i class="icon-caret-down menu"></i>' );
		}
	});

    $('.top-menu-toggle').click(function() {
        $('#menu-top-menu').slideToggle();
        return false;
    });

    $('.main-menu-toggle').click(function() {
        $('#menu-main-menu').slideToggle();
        return false;
    });

    //$('li.menu-item').click(function(e){
        //e.preventDefault();
        //$(this).toggleClass('hover');
    //});

    // if( $(window).width() < 800 ) {
    //     $('.main-menu ul ul li a').prepend( $('<span class="sep">-</span>') );
    // }


	var hps = $('#home-slider').owlCarousel({
        margin:0,
        loop: true,
        autoplay:true,
        items: 1,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }

    });

    $('#testimonials-slider').owlCarousel({
        margin:0,
        nav: true,
        loop: true,
        autoplay:true,
        items: 3,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }

    });

    $(".fancybox").fancybox();

});
