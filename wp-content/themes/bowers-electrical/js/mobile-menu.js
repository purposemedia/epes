jQuery(document).ready(function($){
    var state = "closed";
    var $icon = $("#nav-icon2");
    var $open = $("#menu-open");
    var $close = $("#menu-close");
    var $menu = $('#mobile-menu-menu');
    var $subMenu = $('.mobile-menu__menu > ul > .menu-item > a');



    $icon.click(function(){mobileToggle()});
    $open.click(function(){mobileToggle()});
    $close.click(function(){mobileToggle()});
    $subMenu.click(function(){dropdownToggle(this)});

    $('.sub-menu > .menu-item > a').unbind('click');

    function mobileToggle() {
        $icon.toggleClass('open');
        $menu.toggleClass('open');

        if (state == "closed") {
            //$open.slideUp();
            //$close.slideDown();
            state = "open";
        }

        else if (state == "open") {
            //$open.slideDown();
            //$close.slideUp();
            state = "closed";
        }
    };

    function dropdownToggle(theObject) {
       event.preventDefault(theObject);
        $(theObject).siblings('ul').toggleClass('open');

    };
});