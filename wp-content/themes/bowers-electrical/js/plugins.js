jQuery(document).ready(function($) {

	$(window).load(function(){
		$('.home-services').sync({ selectors : '.des' });
		$('.home-equipment').sync({ selectors : '.image' });
		$('.home-news').sync({ selectors : '.news-block' });
		$('.icons').sync({ selectors : '.sync' });
		$('.footer-awards').sync({ selectors : '.award' });
	});
});