<?php
/* Template Name: Contact */ 
/**
 * The template for displaying a static homepage.
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
get_header(); ?>

		<div id="main" class="main-content" role="main">

			<div class="container">
	
				<?php while ( have_posts() ) : the_post(); ?>

					<div class="contact-details">
					<?php get_template_part( 'content', 'page' ) ?>
					</div>

					<div class="enquiry-form">
						<?php echo do_shortcode(get_field('contact_form')) ?>
					</div>


				<?php endwhile; // end of the loop. ?>	

			</div>

		</div><!--END .main-content-->

<?php get_footer(); ?>