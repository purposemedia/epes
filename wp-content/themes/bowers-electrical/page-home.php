<?php
/* Template Name: Home */ 
/**
 * The template for displaying a static homepage.
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
get_header(); ?>

		<div id="home-slider" class="owl-carousel">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
					if( get_field('banners'))
					{
						while( has_sub_field('banners') )
						{
							$image = get_sub_field('image');
							$title = get_sub_field('title');
							$ctatext = get_sub_field('cta_text');
							$ctalink = get_sub_field('cta_link');
						?>
							<div class="item">
								<img src="<?php echo $image[url]; ?>" alt="<?php echo $image[alt]; ?>" />
								<div class="container">
									<div class="text-outer">
										<div class="text">
											<h1><?php echo $title; ?></h1>
											<p class="banner-cta"><a href="<?php echo $ctalink; ?>"><i class="fa fa-share"></i> <?php echo $ctatext; ?></a></p>
										</div>
									</div>
								</div>
							</div>
						<?php
						}
					}
				?>
			<?php endwhile; // end of the loop. ?>
		</div><!-- # home slider -->

		<div class="home-services">
			<div class="container">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php
						if( get_field('services'))
						{
							while( has_sub_field('services') )
							{
								$image = get_sub_field('image');
								$title = get_sub_field('title');
								$des = get_sub_field('description');
								$link = get_sub_field('page_link');
							?>
								<div class="service">
									<div class="title">
										<a href="<?php echo $link; ?>"><img src="<?php echo $image[url]; ?>" alt="<?php echo $image[alt]; ?>" /></a>
										<h2><?php echo $title; ?></h2>
									</div>
									<div class="des">
										<p><a href="<?php echo $link; ?>"><?php echo $des; ?> <span>&raquo;</span></a></p>
									</div>
								</div>
							<?php
							}
						}
					?>
				<?php endwhile; // end of the loop. ?>
			</div>
		</div><!-- Home services -->

	<!-- 	<div class="home-equipment">
			<div class="container">
				<h3><?php the_field('1st_header'); ?></h3>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php
						if( get_field('equipment'))
						{
							while( has_sub_field('equipment') )
							{
								$image = get_sub_field('image');
								$title = get_sub_field('title');
								$link = get_sub_field('page_link');
							?>
								<div class="equipment">
									<div class="image">
										<a href="<?php echo $link; ?>"><img src="<?php echo $image[url]; ?>" alt="<?php echo $image[alt]; ?>" /></a>
									</div>
									<a href="<?php echo $link; ?>"><h4><?php echo $title; ?></h4></a>
									<a href="<?php echo $link; ?>"><i class="fa fa-share"></i></a>
								</div>
							<?php
							}
						}
					?>
				<?php endwhile; // end of the loop. ?>
			</div>
		</div> --><!-- Home equipment
 -->
		<div class="home-news">
			<div class="container">
				<h5><?php the_field('2nd_header'); ?></h5>
				
				<div>
			    <?php
				$args = array( 'post_type' => array( 'post', 'case-studies' ), 'posts_per_page' => 6, 'cat' => 5 );
				$postsmenu = get_posts( $args );
				foreach($postsmenu as $post) : setup_postdata($post); ?>
				
					<div class="<?php echo get_post_type( $post ) ?>-post news-block">
						<div class="image">
							<a href="<?php the_permalink(); ?>">
								<?php if (get_post_type( $post ) == 'post'): ?>
									<?php the_post_thumbnail('news-post'); ?>
								<?php else: ?>
									<?php the_post_thumbnail('case-post'); ?>
								<?php endif; ?>
							<h3>
								<?php if (get_post_type( $post ) == 'post'): ?>
									News:
								<?php else: ?>
									Case study:
								<?php endif; ?>

								<?php the_title(); ?>
							</h3>
							</a>
						</div>
						<div class="content">
							<a href="<?php the_permalink(); ?>"><?php show_content_snippet($post); ?></a>

							<!-- <p class="link">
							<a href="<?php the_permalink(); ?>">Read more &raquo;</a></p> -->
						</div>
					</div>

				<?php endforeach; ?>
				</div>
			</div>
		</div><!-- Home news -->

		<div class="home-main">
			<div class="container">
				<div class="icons">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php
							if( get_field('icons'))
							{
								while( has_sub_field('icons') )
								{
									$image = get_sub_field('image');
									$title = get_sub_field('text');
								?>
									<div class="icon">
										<div class="image">
											<img src="<?php echo $image[url]; ?>" alt="<?php echo $image[alt]; ?>" />
										</div>
										<p class="sync"><?php echo $title; ?></p>
									</div>
								<?php
								}
							}
						?>
					<?php endwhile; // end of the loop. ?>
				</div>
				<div class="content">
					<?php wp_reset_postdata(); ?>
					<?php while ( have_posts() ) : the_post(); ?>

						<?php the_content(); ?>

					<?php endwhile; // end of the loop. ?>
					<p class="about-link"><a href="<?php echo esc_url( home_url( '/' ) ); ?>about-us/"><i class="fa fa-share"></i> Find out more about us</a></p>
				</div>
			</div>
		</div><!-- Home main -->

		<?php 
		$args = array( 'post_type' => 'testimonial', 'posts_per_page' => 12 );
		$loop = new WP_Query( $args );
		if ($loop->have_posts()):
		?>

		<div class="home-news">
			<div class="container">
				
				<h5>Testimonials</h5>

				<div id="testimonials-slider" class="owl-carousel testimonials-slider">

					 <?php
					while ( $loop->have_posts() ) : $loop->the_post();
						$imgURL = "";
						 if (has_post_thumbnail()) {
						 	$imgURL = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
						 }
					?>

					<div class="slide testimonial-item">

						<div class="testimonial-image" style="background-image:url('<?php echo $imgURL; ?>')"></div>

						<div class="testimonial-content">
							<p>"<?php the_field('testimonial'); ?>"</p>
							<p><b><?php the_field('name'); ?></b></p>
						</div>

					</div>

					<?php endwhile; ?>

				</div>
					
				<div class="testimonial-button">
					<a href="/testimonials/"><i class="fa fa-share" aria-hidden="true"></i>See more testimonials</a>
				</div>

			</div>
		</div>

		<?php 
		endif;
		?>

<?php get_footer(); ?>