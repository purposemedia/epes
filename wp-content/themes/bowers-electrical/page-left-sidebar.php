<?php 
/* Template Name: Left Sidebar */ 
/**
 * The template for displaying all pages with a left sidebar.
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
get_header(); ?>

		<div id="main" class="main-content" role="main">
			<div class="container">

				<aside id="secondary" class="widget-area left" role="complementary">
					<?php dynamic_sidebar( 'page-sidebar' ); ?>
				</aside><!-- #secondary -->

				<div class="content-right">	

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page-left-sidebar' ) ?>

					<?php endwhile; // end of the loop. ?>

				</div>
			</div>
		
		</div><!--END .main-content-->

<?php get_footer(); ?>