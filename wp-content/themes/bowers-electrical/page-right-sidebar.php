<?php 
/* Template Name: Right Sidebar */ 
/**
 * The template for displaying all pages with a right sidebar.
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
get_header(); ?>

		<div id="main" class="main-content" role="main">
			<div class="container">


				<div class="content-left">	

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'page-right-sidebar' ) ?>

					<?php endwhile; // end of the loop. ?>

				</div>
			
				<aside id="secondary" class="widget-area right" role="complementary">
					<?php dynamic_sidebar( 'page-sidebar' ); ?>
				</aside><!-- #secondary -->
			</div>
		</div><!--END .main-content-->

<?php get_footer(); ?>