<?php 
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
get_header(); ?>

		<div id="main" class="main-content" role="main">

			<div class="container">
	
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'content', 'page' ) ?>

				<?php endwhile; // end of the loop. ?>	

			</div>

		</div><!--END .main-content-->

		<div class="row row--primary">
			<div class="inner">

				<?php 
					$form_id = get_field( "form_id" );
					if(!empty($form_id)) {
						echo do_shortcode( '[contact-form-7 id="' . $form_id . '"]' ); 
					} else {
						echo do_shortcode( '[contact-form-7 id="7"]' ); 
					}

				?>
				
			</div>
		</div>


<?php get_footer(); ?>