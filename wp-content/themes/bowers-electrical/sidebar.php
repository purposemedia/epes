<?php 
/**
 * Displays the sidebar content for index.php
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
?>

	<aside id="secondary" class="widget-area" role="complementary">

		<?php if ( ! dynamic_sidebar( 'blog-sidebar' ) ) : ?>

			<aside id="search" class="widget widget_search">
				<?php get_search_form(); ?>
			</aside>

			<aside id="archives" class="widget">
				<h3 class="widget-title"><?php _e( 'Archives', 'melissa' ); ?></h3>
				<ul>
					<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
				</ul>
			</aside>

			<aside id="meta" class="widget">
				<h3 class="widget-title"><?php _e( 'Meta', 'melissa' ); ?></h3>
				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<?php wp_meta(); ?>
				</ul>
			</aside>

		<?php endif; // end sidebar widget area ?>
	</aside><!-- #secondary -->