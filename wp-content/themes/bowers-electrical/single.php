<?php 
/**
 * Displays single posts.
 *
 * @package WordPress
 * @subpackage Melissa
 * @since Melissa 1.0
 */
get_header(); ?>

		<div id="main" class="main-content" role="main">
			<div class="container">


				<div class="content-left">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'content', 'single' ); ?>

						<?php // melissa_content_nav( 'nav-below' ); ?>

						<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || '0' != get_comments_number() )
								comments_template();
						?>

					<?php endwhile; // end of the loop. ?>

				</div>

				<?php get_sidebar(); ?>
			</div>
		</div><!--END .main-content-->

<?php get_footer(); ?>